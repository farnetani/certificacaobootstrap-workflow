# Atividades do curso de Certificação Bootstrap do keven Jesus.

[Link de Acesso do Curso](https://certificacaobootstrap.club.hotmart.com/login.html)

# Start: 12/02/2017

## Módulo: CSS inteligente com SASS

## SASS

## Existem 2 tipos:

    - .scss : modelo convencional parecido com o css.
    - .sass : modelo por identação (tab)

## Variáveis:

$variavel: <valor>;

    - Variável global, local e de escopo.


## Condicional

    - @if { } e @else { }

```
.component {
  width: 200px;
  height: 150px;
  @if $enable {
    background-color: $cor-personalizada;
  }
  @else {
    background-color:yellow;
  }
```

## Nesting

## Herança (@extend): serve para herdar as propriedades

```
.component-navbar {
  width: 100%;
  height: 250px;
  float:left;
  margin-left: 20px;
  padding:10px;
}

.component-header {
  @extend .component-navbar;
  @extend .col-md-6 !optional;
  background-color: #000;
}
```

## Mixin

```
//Se usar um parametro default, não pode ter nenhum parametro depois da virgula sema padrao.
@mixin pessoa ($cor-olho, $altura:120px, $peso: 80px){
  width: $peso;
  height: $altura;
  color: $cor-olho;
}

.joao {
  @include pessoa(#000, 100px, 500px);
}

```

## Placeholder: muito parecido com o @extend, porém ele não aparecerá no código final. Deverá ser herdado.    

```
%componentPlaceHolder {
  width: 600px;
  height: 100px;
  float: left;
}

.perfil {
  @extend %componentPlaceHolder;
}
```

## Funções: criar funções para ser utilizadas

@function porcentagem($porcentagem, $base){
  @return ($porcentagem * $base)/100;
}

.componentFunction{
  position: absolute;
  left: porcentagem(13.5, 1000);
}
Unable to download https://www.atom.io/api/packages/autocomplete-sass/versions/0.1.0/tarball: 400 Bad Request Repository inaccessible

## Camadas
- Ajuste no gulp para ler apenas um arquivo .scss
- Separar as coisas por componente
- Recomenda-se criar uma nova pasta: 
  Exemplo:
- navbar.scss (indice, responsável por importar os arquivos)
    - brand.scss
    - buttons.scss
    - links.scss

- Usar o comando import:
@import "brand.scss";
@import "buttons.scss";
@import "links.scss";

- Criar um arquivo (recomendado) de variáveis. Onde você jogará todas as variáveis:

variaveis.scss

- Jogar ele primeiro no style.scss (onde indicará as variáveis logo de cara).

- Então o style.css (indice), servirá apenas para carregar todos os componentes do projeto. Aí toda vez que vc tiver um componente é só criar uma pasta contendo um arquivo indice contendo os arquivos do componente.



